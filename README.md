# calendar

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Links HTML
```html
<div id="add-to-calendar" class="add-to-calendar-links js-add-to-calendar-links centertext mt20">
    <p class="nomb">Add this event to your calendar:</p>
    <p class="nomb">
        <a 
            download="event.ics" 
            href="data:text/calendar;charset=utf8,BEGIN:VCALENDAR%0AVERSION:2.0%0ABEGIN:VEVENT%0ASUMMARY:East%20End%20Regional%20Meet%20Up%0AURL:https%3A%2F%2Factionnetwork.org%2Fevents%2Feast-end-regional-meet-up%0ADESCRIPTION:https%3A%2F%2Factionnetwork.org%2Fevents%2Feast-end-regional-meet-up%0ALOCATION:Slow%20Blume%20Coffee%20Co-Op.%20420%20W%20Colton%20Ave%5C%2C%20Redlands%5C%2C%20CA%2092374%20-%20US%0AUID:697ad6d4-f9f4-4d16-9c74-18f39f90a1f4%0ADTSTART:20230416T140000%0ADTEND:20230416T160000%0AEND:VEVENT%0AEND:VCALENDAR" target="_blank"
        >Apple</a>
        <a 
            href="https://www.google.com/calendar/render?action=TEMPLATE&amp;text=East%20End%20Regional%20Meet%20Up&amp;dates=20230416T140000/20230416T160000&amp;location=Slow%20Blume%20Coffee%20Co-Op.%20420%20W%20Colton%20Ave%2C%20Redlands%2C%20CA%2092374%20-%20US&amp;details=https%3A%2F%2Factionnetwork.org%2Fevents%2Feast-end-regional-meet-up" 
            target="_blank"
        >Google</a>
        <a 
            download="event.ics" 
            href="data:text/calendar;charset=utf8,BEGIN:VCALENDAR%0AVERSION:2.0%0ABEGIN:VEVENT%0ASUMMARY:East%20End%20Regional%20Meet%20Up%0AURL:https%3A%2F%2Factionnetwork.org%2Fevents%2Feast-end-regional-meet-up%0ADESCRIPTION:https%3A%2F%2Factionnetwork.org%2Fevents%2Feast-end-regional-meet-up%0ALOCATION:Slow%20Blume%20Coffee%20Co-Op.%20420%20W%20Colton%20Ave%5C%2C%20Redlands%5C%2C%20CA%2092374%20-%20US%0AUID:697ad6d4-f9f4-4d16-9c74-18f39f90a1f4%0ADTSTART:20230416T140000%0ADTEND:20230416T160000%0AEND:VEVENT%0AEND:VCALENDAR" 
            target="_blank"
        >Outlook</a>
        <a 
            href="https://outlook.live.com/calendar/0/deeplink/compose?path=/calendar/action/compose&amp;rru=addevent&amp;subject=East%20End%20Regional%20Meet%20Up&amp;startdt=2023-04-16T14:00:00&amp;enddt=2023-04-16T16:00:00&amp;body=https%3A%2F%2Factionnetwork.org%2Fevents%2Feast-end-regional-meet-up&amp;location=Slow%20Blume%20Coffee%20Co-Op.%20420%20W%20Colton%20Ave%2C%20Redlands%2C%20CA%2092374%20-%20US" 
            target="_blank"
        >Outlook Web</a>
        <a 
            href="https://calendar.yahoo.com/?v=60&amp;view=d&amp;type=20&amp;title=East%20End%20Regional%20Meet%20Up&amp;st=20230416T140000&amp;dur=0200&amp;desc=https%3A%2F%2Factionnetwork.org%2Fevents%2Feast-end-regional-meet-up&amp;in_loc=Slow%20Blume%20Coffee%20Co-Op.%20420%20W%20Colton%20Ave%2C%20Redlands%2C%20CA%2092374%20-%20US" 
            target="_blank"
        >Yahoo</a>
    </p>
</div>
```