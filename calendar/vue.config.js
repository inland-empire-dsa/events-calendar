const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  publicPath: "/",
  pwa: {
    name: "Inland Empire DSA Event Calendar",
    workboxOptions: {
      skipWaiting: true
    }
  }
})
