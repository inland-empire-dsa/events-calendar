import Vue from 'vue'
import VueRouter from 'vue-router'
import EventCalendar from '../components/EventCalendar.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: EventCalendar
  },
  {
    path: '/event/:id',
    name: 'event',
    component: EventCalendar
  }
]

const router = new VueRouter({
  routes
})

export default router
